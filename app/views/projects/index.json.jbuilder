json.array!(@projects) do |project|
  json.extract! project, :id, :name_id, :name
  json.url project_url(project, format: :json)
end
